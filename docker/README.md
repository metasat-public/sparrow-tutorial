## Docker container running the example

```bash
# build a fresh lightweight ubuntu 22.04 image
docker build -t sparrow_example .
# run a container
docker run -it --rm -v $(realpath ../example):/home/user/example sparrow_example
```
