#include <sparrow.h>

void gemmAcc(char *M1, char *M2, char *dest, int size)
{
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            char acc = 0;
            for (int k = 0; k < size; ++k) {
                acc += M1[i * size + k] * M2[j * size + k];
            }
            dest[i * size + j] = acc;
        }
    }
}
