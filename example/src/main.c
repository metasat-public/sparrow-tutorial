#include <stdio.h>
#include <stdlib.h>

#define MATRIX_SIZE 32
#define RND_SEED 42

//functions declaration
#define allocate() (char *) malloc(MATRIX_SIZE * MATRIX_SIZE * sizeof(char))

void initialize(char *M, unsigned int seed);
void gemm(char *M1, char *M2, char *dest);
void transpose (char *M, char *MT);
void gemmAcc(char *M1, char *M2, char *dest, int size);
int compare(char *M1, char *M2);


int main()
{
    // Allocate memory for matrices
    char *M_A = allocate();
    char *M_B = allocate();
    char *M_T = allocate();
    char *R_1 = allocate();
    char *R_2 = allocate();

    // Initialize matrices A and B
    initialize(M_A, RND_SEED);
    initialize(M_B, RND_SEED + 1); // Use a different seed for matrixB
                      
    //GEMM
    puts("\nMatrix multiplication");
    gemm(M_A, M_B, R_1);

    // Acclerated GEMM
    puts("Accelerated matrix multiplication");
    transpose(M_B, M_T);
    gemmAcc(M_A, M_T, R_2, MATRIX_SIZE);

    // Compare results
    puts("Comparing the results\n");
    if (compare(R_1, R_2)) puts("Success!");
    else puts("Error: non-matching matrices");

    // Free allocated memory
    free(M_A);
    free(M_B);
    free(M_T);
    free(R_1);
    free(R_2);

    puts("\nSimulation finished");
    puts("Press CTRL+A X to exit");
}

void initialize(char *M, unsigned int seed)
{
    int rows = MATRIX_SIZE;
    int cols = MATRIX_SIZE;
    srand(seed);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            M[i * cols + j] = rand() % 31 - 15; // Values from -15 to 15
        }
    }
}

void gemm(char *M1, char *M2, char *dest)
{
    for (int i = 0; i < MATRIX_SIZE; ++i) {
        for (int j = 0; j < MATRIX_SIZE; ++j) {
            char acc = 0;
            for (int k = 0; k < MATRIX_SIZE; ++k) {
                acc +=  M1[i * MATRIX_SIZE + k] *  M2[k * MATRIX_SIZE + j];
            }
            dest[i * MATRIX_SIZE + j] = acc;
        }
    }
}

void transpose (char *M, char *MT)
{
    for (int i = 0; i < MATRIX_SIZE; ++i) {
        for (int j = 0; j < MATRIX_SIZE; ++j) {
            MT[j * MATRIX_SIZE + i] = M[i * MATRIX_SIZE + j];
        }
    }
}

int compare(char *M1, char *M2)
{
    for (int i = 0; i < MATRIX_SIZE * MATRIX_SIZE; ++i) 
        if (M1[i] != M2[i]) return 0;
    return 1;
}
