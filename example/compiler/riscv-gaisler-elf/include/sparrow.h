/* 
 * This file is a work in progress
 */

#ifndef SPARROW_H
#define SPARROW_H

#include <stdint.h>

/* Architecture format */
#define asm_r3(op, src1, src2, dest) asm (op " %0, %1, %2":"=r"(dest):"r"(src1),"r"(src2))
#define asm_r2(op, src1, dest) asm (op " %0, %1":"=r"(dest):"r"(src1))

/* Utils */
#define __ISUNSIGNED(a) (a>=0 && ~a>=0)

/* SPARROW macros */
//perform C = op2(A op1 B)
#define __sparrow(dest, src1, op1, src2, op2) \
    asm_r3(op1 "_" op2, src1, src2, dest)

//Perform reduction operation
#define __stage2(dest, op, src) \
        if(__ISUNSIGNED(dest)) asm_r2("unop_" op, src, dest); \
        else asm_r2("nop_" op, src, dest)

#define as_vector(var, offset) *((uint64_t *) &var[offset])


typedef uint64_t uint8x8_t;
typedef int64_t int8x8_t;

#endif //SPARROW_H
