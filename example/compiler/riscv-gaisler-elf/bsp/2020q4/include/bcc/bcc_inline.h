/*
 * Copyright (c) 2017, Cobham Gaisler AB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. 
 */

#ifndef __BCC_BCC_INLINE_H_
#define __BCC_BCC_INLINE_H_

/*
 * Implementation of inline functions. This file shall only be included from
 * <bcc/bcc.h>.
 */

#define RISCV_MSTATUS_MIE 0x8

static inline uint32_t riscv_interrupt_disable( void )
{
  unsigned long mstatus;

  __asm__ volatile (
    "csrrc %0, mstatus, 0x8" :
      "=&r" ( mstatus )
  );

  return mstatus & RISCV_MSTATUS_MIE;
}

static inline void riscv_interrupt_enable( uint32_t level )
{
  __asm__ volatile ( "csrrs zero, mstatus, %0" : : "r" ( level ) );
}


static inline int bcc_int_disable(void)
{
        return riscv_interrupt_disable();
}

static inline void bcc_int_enable(int plevel)
{
        riscv_interrupt_enable(plevel);
}

static inline int bcc_timer_tick_init(void)
{
        return bcc_timer_tick_init_period(10 * 1000);
}

#endif

