BCC_COMMON_SOURCES  =
BCC_COMMON_SOURCES += shared/mp.c
BCC_COMMON_SOURCES += shared/isr.c
BCC_COMMON_SOURCES += shared/isr_register_node.c
BCC_COMMON_SOURCES += shared/isr_unregister_node.c
BCC_COMMON_SOURCES += shared/isr_register.c
BCC_COMMON_SOURCES += shared/isr_unregister.c
BCC_COMMON_SOURCES += shared/init.S
BCC_COMMON_SOURCES += shared/dwzero.c
BCC_COMMON_SOURCES += shared/copy_data.c
BCC_COMMON_SOURCES += shared/ambapp.c
BCC_COMMON_SOURCES += shared/ambapp_findfirst_fn.c
BCC_COMMON_SOURCES += shared/lowlevel.S
BCC_COMMON_SOURCES += shared/_exit.S
BCC_COMMON_SOURCES += shared/read.c
BCC_COMMON_SOURCES += shared/sbrk.c
BCC_COMMON_SOURCES += shared/heap.c
BCC_COMMON_SOURCES += shared/times.c
BCC_COMMON_SOURCES += shared/gettimeofday.c
BCC_COMMON_SOURCES += shared/write.c
BCC_COMMON_SOURCES += shared/stubs/stubs.S
BCC_COMMON_SOURCES += shared/stubs/environ.c
BCC_COMMON_SOURCES += shared/handle_exception.c
BCC_COMMON_SOURCES += shared/handle_interrupt.c
BCC_COMMON_SOURCES += shared/ioarea.c

BCC_COMMON_SOURCES += shared/argv.c

BCC_INT_IRQMP_SOURCES  = shared/interrupt/int_irqmp_handle.c
BCC_INT_IRQMP_SOURCES += shared/interrupt/int_irqmp.c
BCC_INT_IRQMP_SOURCES += shared/interrupt/int_irqmp_get_source.c
BCC_INT_IRQMP_SOURCES += shared/interrupt/int_irqmp_init.c
BCC_INT_IRQMP_SOURCES += shared/interrupt/cpu_count.c

BCC_APBUART_SOURCES  = shared/console/con_handle.c
BCC_APBUART_SOURCES += shared/console/con_apbuart.c
BCC_APBUART_SOURCES += shared/console/con_apbuart_init.c

BCC_GPTIMER_SOURCES  = shared/timer/timer_handle.c
BCC_GPTIMER_SOURCES += shared/timer/timer_custom.c
BCC_GPTIMER_SOURCES += shared/timer/timer_gptimer.c
BCC_GPTIMER_SOURCES += shared/timer/timer_gptimer_tick.c
BCC_GPTIMER_SOURCES += shared/timer/timer_gptimer_init.c

COMMON_EXTRA_DATA  = $(BSP_DIR)/linkcmds.memory
COMMON_EXTRA_DATA += $(BCC_PATH)/shared/linkcmds.base
COMMON_EXTRA_DATA += $(BCC_PATH)/shared/linkcmds
COMMON_EXTRA_DATA += $(BCC_PATH)/shared/linkcmds-rom
COMMON_EXTRA_DATA += $(BCC_PATH)/shared/linkcmds-any

