/*
 * Copyright (c) 2017, Cobham Gaisler AB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. 
 */

.ifndef _CONTEXT_I_
_CONTEXT_I_ = 1

        STRUCTDEF isr_ctx
                REGWORD x1
                REGWORD x2
                REGWORD x3
                REGWORD x4
                REGWORD x5
                REGWORD x6
                REGWORD x7
                REGWORD x8
                REGWORD x9
                REGWORD x10
                REGWORD x11
                REGWORD x12
                REGWORD x13
                REGWORD x14
                REGWORD x15
                REGWORD x16
                REGWORD x17
                REGWORD x18
                REGWORD x19
                REGWORD x20
                REGWORD x21
                REGWORD x22
                REGWORD x23
                REGWORD x24
                REGWORD x25
                REGWORD x26
                REGWORD x27
                REGWORD x28
                REGWORD x29
                REGWORD x30
                REGWORD x31
                REGWORD mstatus
                REGWORD mepc
                REGWORD mcause
#if __riscv_flen == 0
#elif __riscv_flen == 32
                WORD   fcsr
                WORD   f0
                WORD   f1
                WORD   f2
                WORD   f3
                WORD   f4
                WORD   f5
                WORD   f6
                WORD   f7
                WORD   f8
                WORD   f9
                WORD   f10
                WORD   f11
                WORD   f12
                WORD   f13
                WORD   f14
                WORD   f15
                WORD   f16
                WORD   f17
                WORD   f18
                WORD   f19
                WORD   f20
                WORD   f21
                WORD   f22
                WORD   f23
                WORD   f24
                WORD   f25
                WORD   f26
                WORD   f27
                WORD   f28
                WORD   f29
                WORD   f30
                WORD   f31
#elif __riscv_flen == 64
                DWORD   fcsr
                DWORD   f0
                DWORD   f1
                DWORD   f2
                DWORD   f3
                DWORD   f4
                DWORD   f5
                DWORD   f6
                DWORD   f7
                DWORD   f8
                DWORD   f9
                DWORD   f10
                DWORD   f11
                DWORD   f12
                DWORD   f13
                DWORD   f14
                DWORD   f15
                DWORD   f16
                DWORD   f17
                DWORD   f18
                DWORD   f19
                DWORD   f20
                DWORD   f21
                DWORD   f22
                DWORD   f23
                DWORD   f24
                DWORD   f25
                DWORD   f26
                DWORD   f27
                DWORD   f28
                DWORD   f29
                DWORD   f30
                DWORD   f31
#endif
        ENDSTRUCT
.endif

