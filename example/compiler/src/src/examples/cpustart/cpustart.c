#include <stdio.h>
#include <stdlib.h>
#include <bcc/bcc.h>

/* some stack for your processors */
uint64_t thestack[16][4096 / 8];

void theentry(int mhartid)
{
        printf("%s: running on processor %d\n", __func__, mhartid);
        exit(0);
}

int main(void)
{
        int ret;
        int ncpu;

        ncpu = bcc_get_cpu_count();
        printf("ncpu=%d\n", ncpu);
        if (ncpu < 2) {
                printf("this example requires at least 2 processors\n");
                exit(0);
        }

        for (int i = 1; i < ncpu; i++) {
                __bcc_startinfo[i].pc = &theentry;
                __bcc_startinfo[i].sp = (uintptr_t) &thestack[i+1][0];
        }

        printf("%s: running on processor 0\n", __func__);
        printf("%s: start processor 1\n", __func__);
        ret = bcc_start_processor(1);
        while (1) {
                ;
        }

        return 0;
}

