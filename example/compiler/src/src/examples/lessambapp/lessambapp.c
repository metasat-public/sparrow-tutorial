/*
 * Override AMBA Plug&Play scanning
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include <bcc/bcc_param.h>

uintptr_t __bcc_plic_handle = 0xf8000000;
uintptr_t __bcc_clint_handle = 0xe0000000;
uintptr_t __bcc_con_handle = 0xfc001000;
uintptr_t __bcc_timer_handle = 0xfc000000;
int __bcc_timer_interrupt = 2;
/* Prevent __bcc_int_init() probing number of CPUs. */
int __bcc_cpu_count = 1;

int main(void)
{
        uint32_t ret;
        clock_t t0, t1;

        puts("");
        printf("%s: __bcc_con_handle            = %p\n", __func__, (void *) __bcc_con_handle);
        printf("%s: __bcc_timer_handle          = %p\n", __func__, (void *) __bcc_timer_handle);
        printf("%s: __bcc_timer_interrupt       = %d\n",   __func__, __bcc_timer_interrupt);
        printf("%s: __bcc_plic_handle           = %p\n", __func__, (void *) __bcc_plic_handle);

        return 0;
}

