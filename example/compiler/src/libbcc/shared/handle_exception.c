#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <bcc/bcc.h>
#include <bcc/bcc_param.h>

#if __riscv_xlen == 32
#define PRINT_REG "0x%08" PRIxPTR
#elif __riscv_xlen == 64
#define PRINT_REG "0x%016" PRIxPTR
#endif

static void printit(
        uintptr_t mstatus,
        uintptr_t mepc,
        uintptr_t mcause,
        struct bcc_exc_ctx *frame
)
{
        printf("%s: enter\n", __func__);
        printf("%s: mstatus = " PRINT_REG "\n", __func__, mstatus);
        printf("%s: mepc    = " PRINT_REG "\n", __func__, mepc);
        printf("%s: mcause  = " PRINT_REG "\n", __func__, mcause);
        printf("%s: frame   = " PRINT_REG "\n", __func__, (uintptr_t) frame);
        for (int i = 0; i < 31; i++) {
                printf("%s:   x[%2d] = " PRINT_REG "\n", __func__, i + 1, frame->x[i]);
        }
}

void __bcc_handle_exception(
        uintptr_t mstatus,
        uintptr_t mepc,
        uintptr_t mcause,
        struct bcc_exc_ctx *frame
)
{
        if (0) {
                printit(mstatus, mepc, mcause, frame);
        }

        if (1) {
                __asm__ volatile ("ebreak");
        } else {
                /* skip instruction causing exception */
                /* FIXME: may not work for C extension */
                frame->mepc += 4;
        }
}

