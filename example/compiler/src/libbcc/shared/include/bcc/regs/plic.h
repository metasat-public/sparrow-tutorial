/*
 * Copyright (c) 2020, Cobham Gaisler AB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. 
 */

#ifndef __PLIC_REGS_H
#define __PLIC_REGS_H

struct plic_context_regs {
        /*
         * The PLIC will mask all PLIC interrupts of a priority less than or
         * equal to threshold.
         */
        uint32_t priority_threshold;
        /* Interrupt Claim/Complete Process for context i */
        uint32_t claim_complete;
        uint32_t reserved_8[1022];
};

struct plic_regs {
        /*
         * interrupt source i priority. i: 0 to 1023 and i=0 does not exist
         * one register per interrupt source
         */
        uint32_t priority[1024];
        /* one pending bit per interrupt source: 32 sources per register */
        uint32_t pending[1024/32];
        uint32_t _rsv0[1024 - 1024/32];
        uint32_t enable[16320][32];
        /* one field per context */
        struct plic_context_regs context[15871];
};

#endif

