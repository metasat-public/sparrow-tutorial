ifeq ($(BSPNAME),)
$(error "BSPNAME not set")
endif

DEFS_DIR :=$(shell dirname  $(lastword $(MAKEFILE_LIST)))
BSP_DIR := $(DEFS_DIR)/bsp/$(BSPNAME)
ifeq ($(BCC_PATH),)
BCC_PATH :=$(DEFS_DIR)
endif
PREFIX=riscv-gaisler-elf-

# CC is overwritten when using clang/llvm
CC=$(PREFIX)gcc
AS=$(CC)
AR=$(PREFIX)ar

ASFLAGS=-I$(BCC_PATH)/shared/inc $(MULTI_FLAGS)
ASFLAGS+=-g

CFLAGS=-std=c99 -g -O3 -Wall -Wextra -pedantic $(MULTI_FLAGS) -I$(BSP_DIR)/include
CFLAGS+=-fno-builtin

ifeq ($(DESTDIR),)
BCC_DISTDIR=$(DEFS_DIR)/dist
else
BCC_DISTDIR=$(DESTDIR)
endif
$(info BCC_DISTDIR=$(BCC_DISTDIR))

ifeq ($(BUILDDIR),)
OUTDIR=build/$(BSPNAME)/$(MULTI_DIR)
else
OUTDIR=$(BUILDDIR)/$(BSPNAME)/$(MULTI_DIR)
endif
$(info OUTDIR=$(OUTDIR))

BCC_OBJDIR=$(OUTDIR)/obj
BCC_LIBDIR=$(OUTDIR)/lib

INSTALL_DATA = install -m 644

