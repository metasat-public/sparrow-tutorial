/* Definitions for NOEL-V running on Cobham Gaisler toolchain
   Copyright (C) 2016 Free Software Foundation, Inc.
   Contributed by Martin Aberg (maberg@gaisler.com).

This file is part of GNU CC.

GNU CC is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU CC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU CC; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* Allow user to specify a BSP not in the default location. */
#undef DRIVER_SELF_SPECS
#define DRIVER_SELF_SPECS \
" \
%{!B: \
  %{qbsp=*:-B %R/bsp/%*} \
  %{!qbsp*:-B %R/bsp/2020q4} \
} \
"

#undef STARTFILE_SPEC
#define STARTFILE_SPEC "first.S.o%s crt0.S.o%s crtbegin%O%s"

#undef LIB_SPEC
#define LIB_SPEC "--start-group -lbcc -lc --end-group"

/* Use the default */
#undef LINK_GCC_C_SEQUENCE_SPEC
#define LINK_GCC_C_SEQUENCE_SPEC "%{!T*: -T linkcmds%s} %G %{!nolibc:%L %G}"

