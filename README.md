# SPARROW Tutorial



## File organization

 - [example/](./example): Files with the example exercise.
 	- [compiler/](./example/compiler/): Compiler to generate SPARROW code for RISC-V.
	- [qemu/](./example/qemu/): QEMU instance for simulating NOEL-V with SPARROW.
	- [src/](./example/src): Source files for the exercise.
	    - [main.c](./example/src/main.c): Main file with the default GEMM.
	    - [gemmAcc.c](./example/src/gemmAcc.c): File with an example GEMM accelerated, modify it to use SPARROW.
	- [Makefile](./example/Makefile): Makefile for the project.
 - [docker/](./docker): Directory to build a Docker container for running the example.

## How to run

Use the included Makefile for compiling and launching a simulation.
```
cd example
make
```
To clean the directory use:
```
make clean
```

Be aware that in order to terminate the qemu simulation you need to use ```CTRL+A X```.

## Run in Docker

See [docker/README.md](./docker/README.md) for information on how to build and run the docker container.

The container automatically compiles and simulates the example. 
The source files are mounted when starting the container, so there is no need to rebuild it everytime.

## Contact us

- [marc.solebonet@bsc.es](mailto:marc.solebonet@bsc.es)
- [jannis.wolf@bsc.es](mailto:jannis.wolf@bsc.es)
- [leonidas.kosmidis@bsc.es](mailto:leonidas.kosmidis@bsc.es)

